# Prueba técnica perfil Full-Stack developer

## Consideraciones generales para la prueba
> **Lo más importante:** si tienes alguna duda de lo que hay que hacer o te gustaría cambiar algo de la prueba para adaptarlo mejor a
tus habilidades, no dudes en contactar con nosotros.

Valoraremos que tu estilo de código se base en estándares como PSR en PHP, o que al menos tu propio estilo sea limpio,
estructurado y homogéneo.
Además, somos fans de los principios [SOLID](https://levelup.gitconnected.com/solid-principles-simplified-php-examples-based-dc6b4f8861f6),
no dudes en aplicarlos en las pruebas.

Por otro lado, nos gusta que el código y los commits cuenten historias ¡y que sean descriptivas!
Procura que tus commits revelen la intencionalidad de cada cambio (atomicidad de las historias) en vez de hacer un único commit.

## Antes de empezar las pruebas
1. Haz un fork privado en Gitlab o en otra plataforma si lo prefieres.
2. Una vez hayas terminado la prueba, invítanos al repo para poder revisarla. :thumbsup:
Correos a donde se puede enviar: alexandrudumitrugorea@gmail.com, formaapp@formaapp.es

## Introducción
El objetivo de esta prueba es demostrar tus capacidades para desarrollar una aplicación web para la gestión de preguntas.
Compuesta de: Frontend, Backend y Backend API HTTP REST para la gestión.

### Historia de usuario para los requerimientos
Como administrador de la aplicación necesito poder listar las preguntas que voy a crear. Además voy a querer poder borrarlas.

## ¿Qué nos gustaría ver en tu prueba?
> Si no tienes alguno de los conocimientos de este apartado, intenta hacer tu código y la solución lo más limpia que puedas,
teniendo en cuenta el mantenimiento futuro del código de todo el stack.

Queremos ver tus aptitudes como programador full stack, por lo que deberías intentar hacer un mínimo de código tanto en
el backend como en el frontend de la prueba.

## Listado de tareas:
1. Crear un proyecto en Laravel, dale el nombre que veas oportuno.
2. Haz que tu aplicación soporte SCSS para crear tus estilos.
3. Crea una maquetación similar para el [listado de preguntas](questions_list.png). <br />
    Recuerda que los estilos no son importantes, pero si que hay que compilar el scss para generar el css. <br />
    La plantilla debería ser creada mediante un blade.
4. Crea la [página de creación de la pregunta](question_create.png). <br />
    La plantilla de esta página es libre.
5. Crear una API HTTP REST el borrado de la pregunta y usalo para darle funcionalidad al botón del listado. Quedando algo similar a este [modal](question_delete_modal.png).
6. Por último crea otro endpoint API para el listado de las preguntas activas, ya se le dará uso en un futuro.

### Apéndice: especificación del API de gestión de procesos
#### Notas generales
* No es necesario implementar un proceso de autentificación en los métodos del API.

#### Eliminar pregunta
`DELETE https://base_url/api/questions/1`

Ejemplo de body petición eliminación sin errores (OK):
```json5
{
  "status": "OK",
  "data": {
    "question_id": "1"
  }
}
```

Ejemplo de body petición eliminación con errores (KO):
```json5
{
  "status": "KO",
  "data": {
    "error_message": "Something when wrong"
  }
}
```

# ¡BUENA SUERTE!
